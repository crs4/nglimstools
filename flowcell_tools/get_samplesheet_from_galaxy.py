#-------------------------
# Export Galaxy history data to a JSON compatible text file
#
# Galaxy host can be retrieved using the GALAXY_HOST environment
# variable
# Galaxy API key can be retrieved using the GALAXY_API_KEY
# environment variable 
#-------------------------

from bioblend.galaxy import GalaxyInstance

import pathset
from automator import illumina_run_dir as ill

import argparse
import os
import sys
from urlparse import urlparse

def log(*args):
  print >> sys.stderr, ' '.join(map(str, args))

def make_parser():
    parser = argparse.ArgumentParser(description='Query Galaxy for the samplesheet for a run')
    parser.add_argument('--galaxy-host', type=str, help='galaxy host (with port)')
    parser.add_argument('--api-key', type=str, help='Galaxy API key')
    parser.add_argument('input_pathset', help="Pathset referencing a run directory")
    parser.add_argument('output_file', help="Path to which to write the samplesheet")
    return parser

def main(argv):
    parser = make_parser()
    args = parser.parse_args(argv)

    try:
        galaxy_host = args.galaxy_host or os.environ['NGLIMS_GALAXY_HOST']
        api_key = args.api_key or os.environ['NGLIMS_GALAXY_API_KEY']
    except KeyError as ke:
        msg = 'No option or environment variables %s set to configure access to the Galaxy server' % ke
        sys.exit(msg)

    pset = pathset.FilePathset.from_file(args.input_pathset)
    if len(pset) != 1:
      raise RuntimeError("Unexpected number of paths for run directory. Expected 1 but got %d" % len(pset))
    run_dir_url = urlparse(pset.get_paths()[0])
    if not run_dir_url.scheme == 'file':
      raise RuntimeError("Need a full URI to the run directory starting with 'file://' (it has to be locally accessible")

    flowcell_id = ill.RunDir(run_dir_url.path).get_run_info().flowcell_id

    log('opening connection to %s' % galaxy_host)
    gi = GalaxyInstance(galaxy_host, key=api_key)

    fc_data = gi.nglims.flowcell_samplesheet(flowcell_id)
    if not fc_data:
        sys.exit("Couldn't find flowcell with id %s" % flowcell_id)

    log('saving to samplesheet %s' % args.output_file)
    with open(args.output_file, 'w') as out_file:
        out_file.write(fc_data)
    log('Job completed')

if __name__ == '__main__':
    main(sys.argv[1:])

# vim: expandtab tabstop=4 shiftwidth=4 autoindent
