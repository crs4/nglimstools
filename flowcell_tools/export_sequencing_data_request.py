#!/usr/bin/env python

from bioblend.galaxy import GalaxyInstance
import nglimsclient
import os, sys, argparse
import csv, logging
import yaml, json, requests, subprocess

sys.path.insert(0, os.path.dirname(__file__))

LOG_FORMAT = '%(asctime)s|%(levelname)-8s|%(message)s'
LOG_DATEFMT = '%Y-%m-%d %H:%M:%S'
LOG_LEVELS = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']
CSV_LABELS = ["FCID", "SampleID", "CustomerSampleID", "SampleProject", "ExportMode", "ExportDetails", "ExportContact",
              "ExportDescription"]

EXPORT_MODE = ["MOUNT", "FTP", "LIBRARY"]


def make_parser():
    parser = argparse.ArgumentParser(description='Export sequencing data request')

    parser.add_argument('--galaxy_host', type=str, help='galaxy host (with port)')
    parser.add_argument('--api_key', type=str, help='Galaxy API key')

    parser.add_argument('--ini_file', type=str,
                        default='/u/sequencing/exports/init_file.yaml',
                        help='Configuration file (yaml)')
    parser.add_argument('--csv_file', type=str, required=True, help='CSV file to import')

    parser.add_argument('--only_check', action='store_true', default=False, help='Check csv file without import it')

    parser.add_argument('--loglevel', type=str, choices=LOG_LEVELS, help='logging level (default: INFO)',
                        default='INFO')
    parser.add_argument('--logfile', type=str, help='log file (default=stderr)')

    return parser


def read_csv(csv_filename):
    global logger
    delimeters = [',', ';', '\t']
    try:
        logger.info('opening csv samplesheet  %s' % csv_filename)
        f = open(csv_filename, 'r')
    except:
        msg = 'ERROR => File %s doesn\'t exist' % csv_filename
        logger.critical(msg)
        sys.exit(msg)

    for delimeter in delimeters:
        f.seek(0)
        csv_request = csv.DictReader(f, CSV_LABELS, delimiter=delimeter)
        if len(list(csv_request)) > 0:
            f.seek(0)
            csv_request = csv.DictReader(f, CSV_LABELS, delimiter=delimeter)
            f.close
            break

    return csv_request


def flowcell_id_exists(id_flowcell):
    exists = apiRest.nglims.exists_flowcell_id(id_flowcell)
    return exists


def sample_id_exists(id_sample):
    exists = apiRest.nglims.exists_sample_id(id_sample)
    return exists


def check_ftp(details):
    return True


def check_mount(details):
    return True

def check_available_size(size,total_size):
    if size < 0: return False
    percent = (size * 100)/total_size
    if percent <= 5: return False
    return  True


def omero_check(id_sample):
    params = {'sample_label': id_sample}
    result = omero_rest_api(ini_file['OMERO_HOST'], ini_file['OMERO_USER'], ini_file['OMERO_PWD'], omero_method='galaxy/api/check_if_sample_exists', other_params=params)
    size = 0
    print result
    for res in result:
        size = size + res.get('size',0)
    return size

def check_request(ids, mode, details, available_size):
    logger.info('checking object ids')
    all_ok = True
    msgs = list()
    size = 0
    for id in ids:
        id = id.encode('utf-8')
        if not flowcell_id_exists(id) and not sample_id_exists(id):
            msgs.append("ERROR => {0}  not exists in ngLIMS".format(id))
            all_ok = False
        else:
            logger.info("{0} exists in ngLIMS".format(id))

        sample_size = 1 #omero_check(id) FIX
        if sample_size == 0:
            msgs.append("ERROR => {0}  not exists in OMERO.BIOBANK".format(id))
            all_ok = False
        else:
            logger.info("{0} exists in in OMERO.BIOBANK".format(id))
            size = size + sample_size

    if not all_ok:
        for m in msgs: logger.warning(m)
        return all_ok

    #checking available size
    logger.info('checking available size')

    if not check_available_size(available_size - size,total_size):
        logger.warning("ERROR => insufficient space on device to export ".format(ids))
        return False
    else:
        available_size = available_size - size


    logger.info('checking export mode')
    if mode not in EXPORT_MODE:
        logger.critical("ERROR => {0} is not a valid export mode".format(mode))
        return False

    logger.info('checking export details')
    if mode in ['FTP']:
        logger.info('checking connection FTP')
        if not check_ftp(details):
            logger.critical("ERROR => {0} is not a valid ftp connection".format(details.values()))
            return False

    if mode in ['MOUNT']:
        logger.info('checking mounting point')
        if not check_mount(details):
            logger.critical("ERROR => {0} is not a valid mounting point".format(details.values()))
            return False
    return True


def retrieve_request_list(csv_request):
    requests = {}
    all_ok = True
    msg = []

    for line in csv_request:
        if sorted(line.values()) != sorted(CSV_LABELS) and len(line.values()) == len(CSV_LABELS):
            description = line['ExportDescription']
            if not requests.has_key(description):
                export_details = []
                if line['ExportDetails'] is not None and len(line['ExportDetails']) > 0:
                    export_details = line['ExportDetails'].split('|')

                requests[description] = {
                    'object_ids': [],
                    'projs_labels': [],
                    'export_mode': line['ExportMode'],
                    'export_details': export_details,
                    'export_status': 'NEW',
                    'export_description': description,
                    'export_contact': line['ExportContact'], }

            if line['SampleID'] not in requests[description]['object_ids']:
                requests[description]['object_ids'].append(line['SampleID'])

            if line['SampleProject'] not in requests[description]['projs_labels']:
                requests[description]['projs_labels'].append(line['SampleProject'])

            # if export_details requests[description]['export_details']]:
            #        msg.append('Request %s: export details for sample %s (%s) different from %s' % (description, line['SampleID'], line['CustomerSampleID'], requests[description]['export_details']))
            #        all_ok = False 

            if line['ExportMode'] not in [requests[description]['export_mode']]:
                msg.append('Request {0}: export mode for sample {1} ({2}) different from {3}'.format(description,
                                                                                                     line['SampleID'],
                                                                                                     line[
                                                                                                         'CustomerSampleID'],
                                                                                                     requests[
                                                                                                         description][
                                                                                                         'export_mode']))
                all_ok = False

            if line['ExportContact'] not in [requests[description]['export_contact']]:
                msg.append('Request {0}: export contact for sample {1} ({2}) different from {3}'.format(description,
                                                                                                        line[
                                                                                                            'SampleID'],
                                                                                                        line[
                                                                                                            'CustomerSampleID'],
                                                                                                        requests[
                                                                                                            description][
                                                                                                            'export_contact']))
                all_ok = False

    if not all_ok:
        msg.append("ERROR => request not valid")
        for error in msg:
            logger.critical(error)
        sys.exit()

    request_list = requests.values()
    return request_list


def get_request_list(args):
    logger.info('getting request list from csv  %s' % args.csv_file)
    csv_request = read_csv(args.csv_file)
    logger.info('checking & retrieving request list')
    request_list = retrieve_request_list(csv_request)
    return request_list


def save_request_list(args, request_list):
    global available_size
    global total_size
    available_size = get_available_size(ini_file)
    total_size = get_total_size(ini_file)

    saved_request = dict()
    for request in request_list:
        if check_request(request['object_ids'], request['export_mode'], request['export_details'], available_size):
            logger.info('saving request row  "%s" %s "%s" %s %s' % (
                request['export_description'], request['object_ids'], request['export_mode'], request['export_details'],
                request['export_contact']))

            if args.only_check:
                logger.info('--only_check enabled: SKIPPED')
                continue

            request['object_ids'] = json.dumps(request['object_ids'])
            request['projs_labels'] = json.dumps(request['projs_labels'])
            request['export_details'] = json.dumps(request['export_details'])

            successfull, rsp = apiRest.nglims.save_export_request(request)
            if successfull:
                logger.info('SUCCESSFUL => request %s saved with id %s' % (request['export_description'], rsp))
                saved_request[rsp] = request
                # data = {'rid' : rsp, 'status' : 'SUCCESSFUL'}
                #apiRest.api_update_export_request_state(data)

            else:
                logger.critical('FAILED => request "%s" not saved: %s' % (request['export_description'], rsp))
        else:
            logger.warning("ERROR => request \"{0}\" not valid: {1}".format(request['export_description'], request))
    return saved_request


def execute_export_request(args, saved_request):
    for id_request, request in saved_request.iteritems():
        fcid = request['export_description'].split('-')[0].strip()
        samples = ",".join(json.loads(request['object_ids']))
        projects = ",".join(json.loads(request['projs_labels']))
        params = "\"id_request={0} fcid={1} samples={2} " \
                 "projects={3}".format(id_request, fcid, samples, projects)

        if request['export_mode'] == 'FTP':
            params = "{}\"".format(params)
            cmd = [ini_file['PLAYBOOK_MANAGE_FTP_EXPORT_REQUEST'], '-e',
                   params, '-v']
        elif request['export_mode'] == 'LIBRARY':
            params = "{}\"".format(params)
            cmd = [ini_file['PLAYBOOK_MANAGE_LIBRARY_EXPORT_REQUEST'], '-e',
                   params, '-v']
        elif request['export_mode'] == 'MOUNT':
            params = "{} mount_path={}\"".format(params,json.loads(request['export_details'])[0])
            cmd = [ini_file['PLAYBOOK_MANAGE_MOUNT_EXPORT_REQUEST'], '-e',
                   params, '-v']
        else:
            pass

        if cmd:
            logger.info(" ".join(cmd))
            output = os.system(" ".join(cmd))  # raises CalledProcessError
            logger.info(output)



def init_logger(args, logging):
    log_level = getattr(logging, args.loglevel)
    kwargs = {
        'format': LOG_FORMAT,
        'datefmt': LOG_DATEFMT,
        'level': log_level}

    if args.logfile:
        kwargs['filename'] = args.logfile
    logging.basicConfig(**kwargs)

    logger = logging.getLogger('export_data')
    return logger


def init_api_galaxy(args):
    try:
        galaxy_host = args.galaxy_host or os.environ['GALAXY_HOST']
        api_key = args.api_key or os.environ['GALAXY_API_KEY']
    except KeyError, ke:
        msg = 'No argument passed and no global variable %s found' % ke
        logger.critical(msg)
        sys.exit(msg)

    logger.info('opening connection to %s' % galaxy_host)
    apiRest = GalaxyInstance(galaxy_host, key=api_key)
    nglimsclient.setup(apiRest)
    return apiRest


def init_config(args):
    # Load YAML configuration file
    logger.info('loading YAML configuration file: {0}'.format(args.ini_file))
    try:
        ini_file = yaml.load(open(args.ini_file))
    except:
        logger.critical('{0} is not a valid YAML configuration file'.format(args.ini_file))
        sys.exit()

    return ini_file


def get_available_size(ini_file):
    cmd = "df | grep %s | awk '$3 ~ /[0-9]+/ { print $4 }'" %(ini_file['FTP_FOLDER'])
    free_size = ssh_cmd('{0}@{1}'.format(ini_file['FTP_USER'], ini_file['FTP_HOST_ALT']), cmd)

    return int(free_size.pop())*1024


def get_total_size(ini_file):
    cmd = "df | grep %s | awk '$3 ~ /[0-9]+/ { print $2 }'" %(ini_file['FTP_FOLDER'])
    free_size = ssh_cmd('{0}@{1}'.format(ini_file['FTP_USER'], ini_file['FTP_HOST_ALT']), cmd)

    return int(free_size.pop())*1024


def ssh_cmd(host, cmd):
    ssh = subprocess.Popen(["ssh", "%s" % host, cmd],
                           shell=False,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
    result = [line.rstrip('\n') for line in ssh.stdout.readlines()]
    error = ssh.stderr.readlines()
    if error != []:
        logger.warning(error)
        sys.exit()
    else:
        return result


def __make_call(args):
    try:
        logger.debug('executing: %s', args)
        output = subprocess.check_output(args)  # raises CalledProcessError
        if output:
            logger.debug('command output: %s', output)
        return output
    except subprocess.CalledProcessError as e:
        logger.debug(e)
        if e.output:
            logger.debug("command output: %s", e.output)
        else:
            logger.debug("no command output available")
        raise 'Error'


def omero_rest_api(omero_host=None, omero_user=None, omero_password=None, omero_method=None, other_params=dict()):
    url = omero_host
    if not url: return list()

    url = 'http://{0}:8080/{1}'.format(url.strip('/'), omero_method)
    payload = {
        'ome_host': omero_host,
        'ome_user': omero_user,
        'ome_passwd': omero_password,
    }

    for k,v in other_params.iteritems():
        payload[k] = v
    try:
        response = requests.post(url, data=payload).json()
    except:
        return list()

    if response['result'] not in ['None']:
        result = response['result']
        return result

    return list()


def main(argv):
    global logger
    global apiRest
    global ini_file

    parser = make_parser()
    args = parser.parse_args(argv)

    # Initializing logger
    logger = init_logger(args, logging)

    # Reading YAML configuration file
    ini_file = init_config(args)

    # Initializing connection to apiGalaxy
    apiRest = init_api_galaxy(args)

    # Getting Request List
    request_list = get_request_list(args)

    # Saving Request List
    saved_request = save_request_list(args, request_list)

    # Executing Export
    execute_export_request(args, saved_request)


if __name__ == '__main__':
    main(sys.argv[1:])