#-------------------------
# Export FC's samplesheet into a csv file
#
# Galaxy host can be retrieved using the GALAXY_HOST environment
# variable
# Galaxy API key can be retrieved using the GALAXY_API_KEY
# environment variable 
#-------------------------

from bioblend.galaxy import GalaxyInstance
import nglimsclient
import argparse
import logging
import os
import sys

LOG_FORMAT = '%(asctime)s|%(levelname)-8s|%(message)s'
LOG_DATEFMT = '%Y-%m-%d %H:%M:%S'
LOG_LEVELS = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']

def make_parser():
    parser = argparse.ArgumentParser(description='Export flowcell samplesheet data')
    parser.add_argument('--loglevel', type=str, choices=LOG_LEVELS,
                        help='logging level (default: INFO)', default='INFO')
    parser.add_argument('--logfile', type=str, help='log file (default=stderr)')
    parser.add_argument('--galaxy_host', type=str, help='galaxy host (with port)')
    parser.add_argument('--api_key', type=str, help='Galaxy API key')
    parser.add_argument('--flowcell_id', type=str, required=True,
                        help='ID of the flowcell that will be exported')
    parser.add_argument('--out_format', type=str, required=True, choices=['samplesheet','export_request'], help='Set the samplesheet output format', default='samplesheet')
    parser.add_argument('--export_mode', type=str, required=False, choices=['FTP','LIBRARY','MOUNT'], help='Set the export mode', default='FTP')
    parser.add_argument('--export_path', type=str, required=False, help='Set the export path', default=None)
    parser.add_argument('--export_contact', type=str, required=False, help='Set the export contact mail', default=None)
    parser.add_argument('--ofile', type=str, help='output file')
    return parser


def make_url(base, service):
        return '/'.join([base, service])


def check_flowcell_id(galaxy_instance, flowcell_id):
    exists = galaxy_instance.nglims.exists_flowcell_id(flowcell_id)
    return exists


def main(argv):
    parser = make_parser()
    args = parser.parse_args(argv)

    log_level = getattr(logging, args.loglevel)
    kwargs = {'format'  : LOG_FORMAT,
              'datefmt' : LOG_DATEFMT,
              'level'   : log_level}
    if args.logfile:
        kwargs['filename'] = args.logfile
    logging.basicConfig(**kwargs)
    logger = logging.getLogger('samplesheet_to_file')

    try:
        galaxy_host = args.galaxy_host or os.environ['GALAXY_HOST']
        api_key = args.api_key or os.environ['GALAXY_API_KEY']
    except KeyError, ke:
        msg = 'No argument passed and no global variable %s found' % ke
        logger.critical(msg)
        sys.exit(msg)

    if args.ofile:
        output_file = args.ofile
    else:
        output_file = 'samplesheet-%s.csv' % args.flowcell_id

    logger.info('opening connection to %s' % galaxy_host)
    gi = GalaxyInstance(galaxy_host, key=api_key)
    nglimsclient.setup(gi)

    if not check_flowcell_id(gi, args.flowcell_id):
        msg = 'Unable to find flowcell with ID %s' % args.flowcell_id
        logger.critical(msg)
        fc_data = msg
    #     sys.exit(msg)
    else:
        logger.info('getting data for flowcell %s' % args.flowcell_id)
        if args.out_format in ['samplesheet']: 
            fc_data = gi.nglims.flowcell_samplesheet(flowcell_id=args.flowcell_id)
        elif args.out_format in ['export_request']: 
            fc_data = gi.nglims.flowcell_samplesheet(flowcell_id=args.flowcell_id, out_format=args.out_format, export_mode=args.export_mode, export_details=args.export_path, export_contact=args.export_contact)
    
    logger.info('saving to samplesheet %s' % output_file)
    with open(output_file, 'w') as out_file:
        out_file.write(fc_data)
    logger.info('Job completed')

if __name__ == '__main__':
    main(sys.argv[1:])
