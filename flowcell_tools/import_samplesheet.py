#!/usr/bin/env python

import os
import io
import sys
import argparse
import csv
import logging
import json

from bioblend.galaxy import GalaxyInstance
import nglimsclient
import requests


sys.path.insert(0, os.path.dirname(__file__))

LOG_FORMAT = '%(asctime)s|%(levelname)-8s|%(message)s'
LOG_DATEFMT = '%Y-%m-%d %H:%M:%S'
LOG_LEVELS = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']
CSV_LABELS = ["FCID", "Lane", "SampleID", "SampleRef", "Index", "Description", "Control", "Recipe", "Operator",
              "SampleProject"]

SAMPLE_FORM = {
    'quantitation_method': u'Quantitation Method',
    'recipe': u'',
    'analysis_validate': u'Standard',
    'analysis': u'Standard',
    'concentration': u'50',
    'validation_results': u'Validation Results',
    'library_type': u'',
    'insert_size_(including_adapters)': u'250',
    'service_names': u'Next gen sequencing',
    'method': u'paired end',
    'multiplex_barcodes': u'{}',
    'description': u'',
    'sample_preparation_method': u'Sample Preparation Method',
    'volume': u'15',
    'genome_build': u'',
    'services': u'7,8',
    'services_check': u'7,8',
    'name': u'',
    'is_multiplex': [],
    'library_construction_method': u'Library Construction Method',
    'genome_build_validate': u'',
    'cycles': u'100',
    'adapters': u''
}

SAMPLES_TO_PROJECT_FORM = {
    'payment_(fund_number)': u'99999',
    'project_name': u'',
    'create_request_button': u'Submit',
    'description': u'dummy descrition',
    'samples': [],
    'form_id': u'samples_to_project'
}

FLOWCELL_FORM = {
    'run_folder': u'',
    'run_notes': u'Run Notes',
    'create_request_button': u'Save',
    'form_id': u'add_to_sequencing_run',
    '_next_gen_prep': 8,
    'cur_id': u'',
    'sorted_samples': u'',
    'ids': u'',
    'state': u'Sequencing',
    '_func_name': 'add_to_sequencing_run',
    '_add_together': True
}

LIGA_URL = "http://10.0.142.60/json/liga.php"

PIPE_CONF_FILE = "/SHARE/USERFS/els7/users/galaxy/conf/pipe/pipe_conf.yaml"

def make_parser():
    parser = argparse.ArgumentParser(description='Import CSV  samplesheet file')

    parser.add_argument('--galaxy_host', type=str, help='galaxy host (with port)')
    parser.add_argument('--api_key', type=str, help='Galaxy API key')

    parser.add_argument('--only-check', action='store_true', default=False,
                        help='Check samplesheet without import it')

    parser.add_argument('--import-from', type=str, choices=['csv', 'liga'],
                        help='Set the frouce from import: csv or liga',
                        default='csv')

    parser.add_argument('--samplesheet', type=str, required=True,
                        help='Samplesheet to import: filename (if import-from=csv) '
                             'or experimentname (if import-from=liga)')

    parser.add_argument('--user-email', type=str,
                        help='user email')

    parser.add_argument('--loglevel', type=str, choices=LOG_LEVELS,
                        help='logging level (default: INFO)',
                        default='INFO')

    parser.add_argument('--logfile', type=str, help='log file (default=stderr)')

    return parser


def main(argv):
    global logger
    global apiRest

    parser = make_parser()
    args = parser.parse_args(argv)

    # Initializing logger
    logger = init_logger(args)

    # Initializing connection to apiGalaxy
    apiRest = init_api_galaxy(args)

    # Reading && formatting samplesheet
    samples, id_flowcell = get_samplesheet(args)

    # Importing samplesheet
    success = import_samplesheet(samples, id_flowcell, args)
    if success:
        emit_event_register_flowcell(id_flowcell)
    sys.exit()



def get_samplesheet(args):
    #global logger
    # get nglims configuration
    logger.info('get nglims configuration')
    nglims_config = get_nglims_config()

    if args.import_from in ['csv']:
        # reading csv samplesheet file
        logger.info('reading csv samplesheet from file %s' % args.samplesheet)
        samplesheet = read_csv_samplesheet(args.samplesheet)

    elif args.import_from in ['liga']:
        # reading csv samplesheet file
        logger.info('reading json samplesheet from ligadb %s' % args.samplesheet)
        samplesheet = read_liga_samplesheet(args.samplesheet)

    else:
        msg = 'WARNING: %s is NOT a valid source for samplesheet' % args.import_from
        log_capture_string = init_capture_log()
        logger.critical(msg)
        send_mail('', False, user_email=args.user_email, msg=dump_capture_log(log_capture_string))
        sys.exit(msg)

    # checking samplesheet
    logger.info('checking samplesheet data')
    all_ok = check_samplesheet(list(samplesheet), nglims_config, args)

    if all_ok:
        msg = '%s is a valid samplesheet' % args.samplesheet
        logger.info(msg)
        if args.only_check:
            send_mail('', True, user_email=args.user_email, msg=msg)
            sys.exit(msg)
    else:
        msg = 'WARNING: %s is NOT a valid samplesheet' % args.samplesheet
        log_capture_string = init_capture_log()
        logger.critical(msg)
        send_mail('', False, user_email=args.user_email, msg=dump_capture_log(log_capture_string))
        sys.exit(msg)

    if args.import_from in ['csv']:
        samplesheet = read_csv_samplesheet(args.samplesheet)

    # formatting samplesheet to import
    logger.info('formatting samplesheet to import')
    samples, id_flowcell = retrieve_samplesheet(samplesheet, nglims_config, args)

    if len(samples) == 0:
        msg = 'ERROR => No samples found in samplesheet csv file'
        log_capture_string = init_capture_log()
        logger.critical(msg)
        send_mail('', False, user_email=args.user_email, msg=dump_capture_log(log_capture_string))
        sys.exit(msg)

    logger.info('found %i samples' % len(samples))
    return samples, id_flowcell


def import_samplesheet(samples, id_flowcell, args):
    # saving samples in nglims
    logger.info('saving samples in nglims')
    lanes, ids, projects = save_samples(samples, args)

    apiRest = init_api_galaxy(args)

    # associating samples to project
    logger.info('associating samples to projects')
    samples_to_projects(ids, projects)

    # setting samples state in 'sequencing mode'
    logger.info("setting samples state in 'sequencing mode'")
    move_samples_to_sequencing_queue(ids)

    # building flowcell && creating sequencing run
    logger.info("building flowcell && creating sequencing run")
    success = add_samples_to_sequencing_run(lanes, ids, id_flowcell)

    global log_capture_string
    log_capture_string = init_capture_log()
    if success:
        logger.info(
            "SUCCESS => Samplesheet %s is now on Galaxy nglims with flowcell id %s" % (args.samplesheet, id_flowcell))
    else:
        logger.info(
            "FAILED => Samplesheet %s with flowcell id %s is NOT on Galaxy nglims " % (args.samplesheet, id_flowcell))

    send_mail(id_flowcell, success, user_email=args.user_email, msg=dump_capture_log(log_capture_string))

    return success

#
# check if is a valid samplesheet
#
def check_samplesheet(samplesheet, nglims_config, args):

    try:
        all_ok = True
        header = True
        msg = []
        for line in samplesheet:

            # checking header
            if header and sorted(line.values()) != sorted(CSV_LABELS):
                header = False
            elif not header and sorted(line.values()) != sorted(CSV_LABELS):
                header = False
            else:
                continue

            # checking rows
            if not header and sorted(line.values()) != sorted(CSV_LABELS):

                # checking consistency
                if sorted(line.keys()) != sorted(CSV_LABELS):
                    msg.append('ERROR => Samplesheet is not well formatted')
                    all_ok = False

                # chiecking if flowcell id exists
                if flowcell_id_exists(line['FCID'].strip()):
                    if 'ERROR => Flowcell ID %s exists' % line['FCID'] not in msg:
                        msg.append('ERROR => Flowcell ID %s exists' % line['FCID'])
                    all_ok = False

                # checking barcodes
                if not check_barcode(line['Index'], nglims_config['barcodes']):
                    msg.append('ERROR => %s is not a valid barcode' % line['Index'])
                    all_ok = False

                # checkin recipes
                if line['Recipe'] not in nglims_config['recipes']:
                    msg.append('ERROR => %s is not a valid recipe' % line['Recipe'])
                    all_ok = False

                # checkin adapters
                if line['Description'] not in nglims_config['adapters']:
                    msg.append('ERROR => %s is not a valid adapter' % line['Description'])
                    all_ok = False

                # checking references
                if line['SampleRef'].lower() not in nglims_config['genome_build'].keys():
                    msg.append('ERROR => %s is not a valid reference' % line['SampleRef'])
                    all_ok = False


        # check lenght of samplesheet
        if len(samplesheet) < 8:
            msg.append('ERROR => Only %s in samplesheet' % len(samplesheet))
            all_ok = False

        if not all_ok:
            log_capture_string = init_capture_log()
            for error in msg:
                logger.critical(error)
            send_mail('', False, user_email=args.user_email, msg=dump_capture_log(log_capture_string))
            sys.exit()
        return all_ok

    except ValueError:
        msg = 'ERROR => Samplesheet is not valid'
        logger.critical(msg)
        return False


def check_barcode(index, barcodes):
    index = index.strip()
    if "none" != index.lower() and "na" != index.lower() and len(index) > 0:
        if len(index) < 6:
            return False

        for key in barcodes.keys():
            if index[:6] == key[:6]:
                return True
        return False
    return True


#
# retrieve data from samplesheet to save it in nglims
#
def retrieve_samplesheet(csv_samplesheet, nglims_config, args):
    flowcell_id = None
    samples = dict()
    try:
        for line in csv_samplesheet:
            if sorted(line.values()) != sorted(CSV_LABELS):
                key = line["SampleID"] + "|" + line["Index"]
                this_lane = line["Lane"]
                if key in samples:
                    samples[key]['lanes'].append(this_lane)
                else:
                    if flowcell_id is None:
                        flowcell_id = retrieve_fcid(line['FCID'].strip())
                        logger.info('flowcell_id  = %s' % flowcell_id)

                    samples[key] = dict()

                    samples[key] = {
                        'description': line["SampleID"],
                        'recipe': line["Recipe"],
                        'lanes': [this_lane],
                        'multiplex_barcodes': retrieve_barcode(line["Index"].strip(), nglims_config['barcodes']),
                        # 'multiplex_barcodes' : '{}',
                        'id': 0,
                        'name': '',
                        'adapters': line['Description'],
                        'genome_build': retrieve_genome_build(line["SampleRef"].strip(), nglims_config['genome_build']),
                        'genome_build_validate': retrieve_genome_build(line["SampleRef"].strip(),
                                                                       nglims_config['genome_build']),
                        'operator': retrieve_operator(line["Operator"]),
                        'project': retrieve_project(line['SampleProject']),
                    }
                    if samples[key]['multiplex_barcodes'] != '{}':
                        samples[key]['is_multiplex'] = '["true","true"]'
                    else:
                        samples[key]['is_multiplex'] = '["false","false"]'

    except ValueError:
        msg = 'ERROR => File is not a samplesheet csv file'
        log_capture_string = init_capture_log()
        logger.critical(msg)
        send_mail('', False, user_email=args.user_email, msg=dump_capture_log(log_capture_string))
        sys.exit(msg)
    return samples, flowcell_id


def retrieve_fcid(fcid):
    return fcid.strip()[-9:]


def retrieve_operator(operator):
    api_key = apiRest.nglims.get_api_key(operator)
    return api_key


def retrieve_genome_build(sampleref, genome_build):
    if sampleref.lower() in genome_build.keys():
        return genome_build[sampleref.lower()]['label']
    else:
        return ''


def retrieve_recipe(recipe, recipes):
    for key in recipes.keys():
        if key in recipe.lower():
            return recipes[key]


def retrieve_barcode(index, barcodes):
    multiplexed_barcodes = '{}'
    if index.lower() != 'none' and index.lower() != 'na' and len(index) > 0:
        if len(index) < 6:
            return multiplexed_barcodes

        for key in barcodes.keys():
            if index[:6] == key[:6]:
                label = barcodes[key]['label']
                multiplexed_barcodes = '{"barcodes":[["NEW^^barcode","%s"]],"barcode_type" : "%s", "parent_id" : "", ' \
                                       '"not_filled": [], "not_named" : false}' \
                                       % (label, barcodes[key]['type'])
    return multiplexed_barcodes


def retrieve_lanes(lanes):
    str_lanes = ''
    for i in range(1, 9):
        klane = "lane%s" % i
        if klane in lanes:
            str_lanes = '%s"%s":%s,' % (str_lanes, klane, json.dumps(lanes[klane]))
    str_lanes = '{%s}' % str_lanes[0:len(str_lanes) - 1]
    lanes = unicode(str_lanes.replace(" ", ""))
    return lanes

def retrieve_project(project):
    if len(project) > 0:
        project = project.replace(' ','-')
    return project

def retrieve_nglims_config(nglims_config):
    # getting barcodes
    nglims_barcodes = {}
    for entry in nglims_config['barcodes']:
        barcodes = entry['data']
        for barcode in barcodes:
            for k, v in barcode.iteritems():
                if not v in nglims_barcodes:
                    nglims_barcodes[v] = {'label':  "%s : %s" % (k, v), 'type': entry['name']}

    # getting recipes and adapters
    nglims_recipes = []
    nglims_adapters = []
    nglims_requests = nglims_config['requests']
    for req in nglims_requests:
        if req['name'] == 'Next gen sequencing':
            inputs = req['form']['inputs']
            for row in inputs:
                if row['label'] == 'Recipe':
                    nglims_recipes = row['type']

        if req['name'] == 'Library construction':
            inputs = req['form']['inputs']
            for row in inputs:
                if row['label'] == 'Adapters':
                    nglims_adapters = row['type']

    # getting genome build
    nglims_genome_build = {}
    for ref in nglims_config['genome_build']:
        nglims_genome_build[ref['name'].lower()] = {'code': ref['code'], 'label': ref['label']}

    nglims_config = {'barcodes': nglims_barcodes, 'recipes': nglims_recipes, 'adapters': nglims_adapters,
                     'genome_build': nglims_genome_build}
    return nglims_config


#
# save samplesheet on nglims
#
def save_samples(samples, args):
    lanes = dict()
    projects = dict()
    ids = []
    for key, sample in samples.iteritems():
        # preparing form to post data
        if sample['operator'] is not None:
            galaxy_host = args.galaxy_host or os.environ['GALAXY_HOST']
            apiRest = GalaxyInstance(galaxy_host, key=sample['operator'])
            nglimsclient.setup(apiRest)
        else:
            galaxy_host = args.galaxy_host or os.environ['GALAXY_HOST']
            api_key = args.api_key or os.environ['GALAXY_API_KEY']
            apiRest = GalaxyInstance(galaxy_host, key=api_key)
            nglimsclient.setup(apiRest)

        del sample['operator']

        this_project = sample['project']
        del sample['project']

        if not this_project in projects:
            projects[this_project] = []

        form_sample = SAMPLE_FORM
        sample['name'] = apiRest.nglims.get_new_sample_name()
        for k, v in sample.iteritems():
            if k not in 'lanes' and k not in 'id':
                form_sample[k] = v
        # saving sample

        logger.info('saving sample %s with name %s | lanes: %s | barcode: %s | recipe: %s | adapters: %s' % (
            sample['description'], sample['name'], ','.join(sample['lanes']), sample['multiplex_barcodes'],
            sample['recipe'], sample['adapters']))
        # print form_sample
        id_sample = apiRest.nglims.save_sample(form_sample)
        if id_sample:
            # sample saved
            logger.info('saved with id: %s' % id_sample)
            ids.append('%s' % id_sample)
            if id_sample not in projects[this_project]:
                projects[this_project].append('%s' % id_sample)

            sample['id'] = id_sample
            samples[key] = sample
            for lane in sample["lanes"]:
                klane = "lane%s" % lane
                if klane in lanes:
                    lanes[klane].append("%s" % id_sample)
                else:
                    lanes[klane] = ["%s" % id_sample]
        else:
            log_capture_string = init_capture_log()
            # error while saving sample
            logger.critical('error while saving sample %s' % sample['description'])
            # deleting other samples saved
            logger.info('deleting other samples saved')
            delete_samples(ids)
            msg = 'ERROR => error while saving sample %s' % sample['description']
            send_mail('', False, user_email=args.user_email, msg=dump_capture_log(log_capture_string))
            sys.exit(msg)
    lanes = retrieve_lanes(lanes)

    return lanes, ids, projects


def samples_to_projects(ids, projects):
    for key, value in projects.items():
        logger.info('adding samples with ids %s on project %s' % (value, key))
        form_samples_to_project = SAMPLES_TO_PROJECT_FORM
        # preparing form to post data
        form_samples_to_project['samples'] = value
        form_samples_to_project['project_name'] = key
        # run api call
        success, messages = apiRest.nglims.samples_to_project(form_samples_to_project)

        if not success:
            # error while associating samples to project
            logger.critical('error while associating samples to project  %s' % form_samples_to_project['project_name'])
            # deleting other samples saved
            logger.info('deleting other samples saved')
            delete_samples(ids)
            msg = 'ERROR => error while associating samples to project  %s' % form_samples_to_project['project_name']
            sys.exit(msg)

    return True


def move_samples_to_sequencing_queue(ids):
    # from 'awaiting' state to 'pre-sequencing' state
    new_state_0 = apiRest.nglims.move_samples_to_next_queue(ids)
    # from 'pre-sequencing' state to 'sequencing' state
    new_state_1 = apiRest.nglims.move_samples_to_next_queue(ids)

    if new_state_0 not in ['Pre-sequencing quantitation'] or new_state_1 not in ['Sequencing']:
        # error while setting samples state in 'sequencing mode'
        logger.critical("error while setting samples state in 'sequencing mode'")
        # deleting other samples saved
        logger.info('deleting other samples saved')
        delete_samples(ids)
        msg = "ERROR => error while setting samples state in 'sequencing mode'"
        sys.exit(msg)


def add_samples_to_sequencing_run(lanes, ids, id_flowcell):
    form_flowcell = FLOWCELL_FORM
    # preparing form to post data
    form_flowcell['ids'] = ','.join(ids)
    form_flowcell['sorted_samples'] = lanes
    form_flowcell['run_folder'] = str(id_flowcell)
    form_flowcell['cur_id'] = str(ids[0])
    # run api call
    apiRest.nglims.add_samples_to_sequencing_run(form_flowcell)

    if flowcell_id_exists(id_flowcell):
        return True

    return False


def delete_samples(ids):
    # deleting other samples saved
    for sample_id in ids:
        logger.info('deleting sample with id %s ' % sample_id)
        apiRest.nglims.delete_sample(sample_id)


def send_mail(id_flowcell, success, user_email=None, msg=None):
    data = dict(id_flowcell=id_flowcell, status=success, user_email=user_email, msg=msg)
    apiRest.nglims.confirm_import_samplesheet(data)


def flowcell_id_exists(id_flowcell):
    fcid = retrieve_fcid(id_flowcell)

    return apiRest.nglims.exists_flowcell_id(fcid)


def get_nglims_config():
    nglims_config = apiRest.nglims.get_nglims_config()

    if nglims_config is None:
        msg = 'ERROR => No nglims config file found'
        logger.critical(msg)
        sys.exit(msg)

    return retrieve_nglims_config(nglims_config)


def read_csv_samplesheet(csv_filename):
    #global logger
    delimeters = [',', ';', '\t']
    try:
        logger.info('opening csv samplesheet  %s' % csv_filename)
        f = open(csv_filename, 'r')

    except ValueError:
        msg = 'ERROR => File %s doesn\'t exist' % csv_filename
        logger.critical(msg)
        sys.exit(msg)

    for delimeter in delimeters:
        f.seek(0)
        csv_samplesheet = csv.DictReader(f, CSV_LABELS, delimiter=delimeter)
        if len(list(csv_samplesheet)) > 0:
            f.seek(0)
            csv_samplesheet = csv.DictReader(f, CSV_LABELS, delimiter=delimeter)
            f.close
            break

    return csv_samplesheet


def read_liga_samplesheet(expname):
    headers = {'content-type': 'application/json'}
    payload = dict(method="getSampleSheet", params=[expname], jsonrpc="2.0", id=0)

    response = requests.post(
        LIGA_URL, data=json.dumps(payload), headers=headers).json()

    if response['result'] not in ['None']:
        return response['result']
    else:
        msg = 'ERROR => Samplesheet %s doesn\'t exist' % expname
        logger.critical(msg)
        sys.exit(msg)


def init_logger(args):
    log_level = getattr(logging, args.loglevel)
    kwargs = dict(format=LOG_FORMAT, datefmt=LOG_DATEFMT, level=log_level)

    if args.logfile:
        kwargs['filename'] = args.logfile
    logging.basicConfig(**kwargs)

    return logging.getLogger('csv_to_samplesheet')


def init_capture_log():
    log_capture_str = io.BytesIO()
    ch = logging.StreamHandler(log_capture_str)
    ch.setLevel(logging.DEBUG)
    logger.addHandler(ch)
    return log_capture_str


def dump_capture_log(log_capture_str):
    log_contents = log_capture_str.getvalue()
    log_capture_str.close()
    return log_contents


def emit_event_register_flowcell(fcid):
    cmd = "emit_event new_samplesheet_on_nglims \"flowcell_id: {}\" -c {}".format(fcid, PIPE_CONF_FILE)
    os.system(cmd)
    #output = os.system(cmd)
    #logger.info(output)

def init_api_galaxy(args):
    try:
        galaxy_host = args.galaxy_host or os.environ['GALAXY_HOST']
        api_key = args.api_key or os.environ['GALAXY_API_KEY']
    except KeyError, ke:
        msg = 'No argument passed and no global variable %s found' % ke
        logger.critical(msg)
        sys.exit(msg)

    logger.info('opening connection to %s' % galaxy_host)
    gi = GalaxyInstance(galaxy_host, key=api_key)

    return nglimsclient.setup(gi)


if __name__ == '__main__':
    main(sys.argv[1:])
