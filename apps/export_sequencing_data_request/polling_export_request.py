import os
import subprocess


class PollingExportRequest:
    def __init__(self, api, conf, logger):
        self._conf = conf
        self._logger = logger
        self._orione = api.orione
        self._nglims = api.nglims
        self._libraries = self._orione.gi.libraries
        self._export_request = None
        self._params = dict()

    def polling(self, export_request, params=dict()):
        self._export_request = export_request
        self._params = params

        self._logger.info("Preparing to polling")
        prepared_data = self._prepare_polling(export_mode=self._export_request.get_export_mode())

        if self._check_jobid(rsync_jobid=self._params.get('rsync_jobid')):
            polling_files = self._get_polling_files(export_mode=self._export_request.get_export_mode(),
                                                    rsync_script=self._params.get('rsync_script'))

            self._polling(export_mode=self._export_request.get_export_mode(),
                          polling_files=polling_files,
                          prepared_data=prepared_data)

            self._ultimate(export_mode=self._export_request.get_export_mode())

    def _polling(self, export_mode, polling_files, prepared_data):
        all_done = False

        while not all_done:
            done = True
            for dest_path in polling_files:

                if export_mode in ['FTP']:
                    cmd = "ls {}".format(prepared_data.get('dest_folder'))
                    folder_content = self._ssh_cmd("{}@{}".format(self._params['conf'].get('user'),
                                                          self._params['conf'].get('host')),
                                           cmd)
                    dest_path = dest_path.split(':')[1].replace(prepared_data.get('dest_folder'), '').replace('/','')
                    if dest_path not in folder_content:
                        done = False

                elif export_mode in ['MOUNT']:
                    if not os.path.exists(dest_path):
                        done = False

                elif export_mode in ['LIBRARY']:
                    if dest_path.endswith('.gz'):
                        unzip_path = dest_path.replace('.gz', '')

                        if not os.path.exists(dest_path) and not os.path.exists(unzip_path):
                            done = False
                        elif os.path.exists(dest_path):
                            done = False
                            self._logger.info("found {}".format(os.path.basename(dest_path)))
                            self._logger.info("gunzipping {0}".format(os.path.basename(dest_path)))
                            cmd = "gunzip {}".format(dest_path)
                            g_unzip = subprocess.check_output(cmd,
                                                              stderr=subprocess.STDOUT,
                                                              shell=True).strip()
                            self._logger.info(g_unzip)
                            self._logger.info('importing in library {}'.format(unzip_path))
                            status = self._libraries.upload_from_galaxy_filesystem(
                                library_id=prepared_data.get('library_id'),
                                filesystem_paths=unzip_path,
                                folder_id=prepared_data.get('folder_id'),
                                file_type='fastqsanger',
                                link_data_only='link_to_files')

                            self._logger.info(status)

                all_done = done

    def _ultimate(self, export_mode):

        self._logger.info("Setting request status as SUCCESS")
        self._export_request.set_as_successfully()

        conf_export = self._conf.get_section('export')

        if export_mode in ['LIBRARY'] and 'library' in conf_export:
            self._params['conf'] = conf_export.get('library', None)
            pass

        elif export_mode in ['MOUNT'] and 'mount' in conf_export:
            self._params['conf'] = conf_export.get('mount', None)
            pass

        elif export_mode in ['FTP'] and 'ftp' in conf_export:
            self._params['conf'] = conf_export.get('ftp', None)
            # change permission
            cmd = "sudo chown {} {}".format(self._params.get('ftp_customer'),
                                            os.path.join(self._params['conf'].get('folder'),
                                                         self._params.get('ftp_customer')))
            self._logger.info(cmd)
            self._ssh_cmd("{}@{}".format(self._params['conf'].get('user'),
                                         self._params['conf'].get('host')),
                          cmd)

        self._send_mail(export_mode=export_mode)

    def _get_polling_files(self, export_mode, rsync_script=None):
        files = list()

        if rsync_script and os.path.exists(rsync_script):
            f = open(rsync_script, 'r')
            script_rows = f.readlines()
            f.close()

            if export_mode in ['FTP']:
                files = [row.strip().split(' ')[4] for row in script_rows if len(row.strip().split(' ')) == 5]

            elif export_mode in ['LIBRARY']:
                files = [row.strip().split(' ')[3] for row in script_rows if len(row.strip().split(' ')) == 4]

            elif export_mode in ['MOUNT']:
                files = [row.strip().split(' ')[3] for row in script_rows if len(row.strip().split(' ')) == 4]

        return files

    def _check_jobid(self, rsync_jobid=None):
        if rsync_jobid:
            self._logger.info("Checking for job {}".format(rsync_jobid))
            cmd = ['qstat | grep {} | sed \'s,^ *,,; s, *$,,\' | cut -d \' \' -f1'.format(rsync_jobid)]
            jobid = subprocess.check_output(cmd,
                                            stderr=subprocess.STDOUT,
                                            shell=True).strip()

            return jobid == rsync_jobid

        return False

    def _prepare_polling(self, export_mode):

        prepared = dict()
        conf_export = self._conf.get_section('export')

        if 'LIBRARY' in export_mode and 'library' in conf_export:
            self._params['conf'] = conf_export.get('library', None)
            # Getting library_is and folder_id
            library_name = self._params['conf'].get('name', None)
            folder_name = self._export_request.get_export_fcid()

            library_id, folder_id = self._get_library_folder_ids(library_name=library_name,
                                                                 folder_name=folder_name)

            prepared = dict(library_id=library_id, folder_id=folder_id)

        elif 'FTP' in export_mode and 'ftp' in conf_export:
            self._params['conf'] = conf_export.get('ftp', None)
            dest_folder = os.path.join(self._params['conf'].get('folder'),
                                       self._params['ftp_customer'],
                                       self._export_request.get_export_fcid())

            prepared = dict(dest_folder=dest_folder)

        elif 'MOUNT' in export_mode and 'mount' in conf_export:
            pass

        return prepared

    def _send_mail(self, export_mode):
        conf_export = self._conf.get_section('export')

        if export_mode in ['FTP']:
            self._params['conf'] = conf_export.get('ftp', None)
            body = "FASTQ File about sample(s) {}\n" \
                   "from flowcell {}\n" \
                   "are now available on CRS4 FTP server\n" \
                   "{}\n\n" \
                   "User: {}" \
                   "Password: {}\n\n""" \
                   "Instructions for CRS4 FTP server:""" \
                   "{}".format(self._export_request.get_samples().values(),
                               self._export_request.get_export_fcid(),
                               self._params['conf'].get('host_alt'),
                               self._params['ftp_customer'],
                               self._params['ftp_password'],
                               self._params['conf'].get('wiki'))

            subject = "FCID {} - FASTQ File now available on CRS4 FTP server".format(
                self._export_request.get_export_fcid())

        elif export_mode in ['LIBRARY']:
            self._params['conf'] = conf_export.get('library', None)
            body = "FASTQ File about sample(s) {0}\n" \
                   "from flowcell {1}\n" \
                   "are now available on CRS4 ORIONE Library\n" \
                   "{2}/{1}\n\n" \
                   " ".format(self._export_request.get_samples().values(),
                              self._export_request.get_export_fcid(),
                              self._params['conf'].get('name')
                              )

            subject = "FCID {} - FASTQ File now available on CRS4 ORIONE Library".format(
                self._export_request.get_export_fcid())

        elif export_mode in ['MOUNT']:
            self._params['conf'] = conf_export.get('library', None)
            body = "FASTQ File about sample(s) {}\n" \
                   "from flowcell {}\n" \
                   "are now available on path\n" \
                   "{}\n\n" \
                   " ".format(self._export_request.get_samples().values(),
                              self._export_request.get_export_fcid(),
                              self._params['mount_path']
                              )

            subject = "FCID {} - FASTQ File now available on CRS4 File System".format(
                self._export_request.get_export_fcid())

        export_contact = self._export_request.get_export_contact()
        if export_contact:
            self._nglims.send_mail(receivers=[export_contact], body=body, subject=subject)
        else:
            self._nglims.send_mail(body=body, subject=subject)

    def _get_library_folder_ids(self, library_name, folder_name):
        hl = self._libraries.get_libraries()

        library_ids = [lib['id'] for lib in hl if lib['name'] == library_name]

        if len(library_ids) == 0:
            self._logger.critical("Missing library {}".format(library_name))
            raise

        library_id = library_ids.pop()
        folder = self._libraries.get_folders(library_id, name=u"/{}".format(folder_name))

        if len(folder) == 0:
            folder = self._libraries.create_folder(library_id, folder_name)

        folder_id = folder[0].get('id', None)

        return library_id, folder_id

    def _ssh_cmd(self, host, cmd):
        ssh = subprocess.Popen(["ssh", host, cmd],
                               shell=False,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

        result = [line.rstrip('\n') for line in ssh.stdout.readlines()]
        error = ssh.stderr.readlines()

        if error:
            self._logger.warning(error)
            raise
        else:
            return result
