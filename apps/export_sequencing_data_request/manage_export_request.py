#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import alta
from export_request import ExportRequest
from run_export_request import RunExportRequest
from polling_export_request import PollingExportRequest

sys.path.insert(0, os.path.dirname(__file__))


def make_parser():
    parser = argparse.ArgumentParser(description='Manage export request')

    parser.add_argument('--config-file', '-c', type=str,
                        default='init_file.yaml', help='yaml config file')

    parser.add_argument('--id-request', type=str, required=False,
                        help='Export Request nglims ID')

    parser.add_argument('--ftp-customer', type=str, help='')
    parser.add_argument('--ftp-password', type=str, help='')
    parser.add_argument('--mount-path', type=str, help='')

    parser.add_argument('--polling-rsync-script', type=str, help='')
    parser.add_argument('--polling-rsync-jobid', type=str, help='')

    parser.add_argument('--loglevel', type=str, choices=alta.LOG_LEVELS,
                        help='logging level (default: INFO)', default='INFO')
    parser.add_argument('--logfile', type=str,
                        help='log file (default=stderr)')

    return parser


def main(argv):
    def check_if_path_exists(path, logger):
        if not os.path.exists(path):
            logger.error("{} doesn't exists".format(path))
            sys.exit()

    parser = make_parser()
    args = parser.parse_args(argv)

    # Initializing logger
    logger = alta.a_logger('main', level=args.loglevel,
                           filename=args.logfile)

    if args.config_file:
        check_if_path_exists(args.config_file, logger)

    # Load YAML configuration file
    conf = alta.ConfigurationFromYamlFile(args.config_file)

    # Initializing connection to ALTA
    api = Client(conf=conf, logger=logger)

    # Getting ExportRequest for given id-request
    export_request = ExportRequest(api=api, logger=logger)
    export_request.init(id_request=args.id_request)

    if export_request.get_export_request() \
            and not args.polling_rsync_script \
            and not args.polling_rsync_jobid:

        run_export_request = RunExportRequest(api=api, conf=conf, logger=logger)
        run_export_request.run(export_request=export_request,
                               params=dict(ftp_customer=args.ftp_customer,
                                           ftp_password=args.ftp_password,
                                           mount_path=args.mount_path,
                                           config_file=args.config_file)
                               )
    elif export_request.get_export_request() \
            and args.polling_rsync_script \
            and args.polling_rsync_jobid:

        polling_export_request = PollingExportRequest(api=api, conf=conf, logger=logger)
        polling_export_request.polling(export_request=export_request,
                                       params=dict(ftp_customer=args.ftp_customer,
                                                   ftp_password=args.ftp_password,
                                                   mount_path=args.mount_path,
                                                   rsync_script=args.polling_rsync_script,
                                                   rsync_jobid=args.polling_rsync_jobid))


class Client:
    def __init__(self, conf, logger):
        self.conf = conf
        self.logger = logger
        self.omero = self.__init_omero()
        self.nglims = self.__init_nglims()
        self.irods = self.__init_irods()
        self.orione = self.__init_orione()

    # Initializing connection to NgLims
    def __init_nglims(self):
        conf_nglims = self.conf.get_galaxy_section(subsection='nglims')
        if conf_nglims:
            galaxy_host = conf_nglims.get('url', None)
            api_key = conf_nglims.get('api_key', None)

            # NgLims client init
            gi = alta.galaxy.NGLims(galaxy_host=galaxy_host, api_key=api_key)
            return gi.gi.nglims
        else:
            self.logger.error('Galaxy NgLims config not found')
            sys.exit()

    # Initializing connection to Orione
    def __init_orione(self):
        conf_orione = self.conf.get_galaxy_section(subsection='orione')
        if conf_orione:
            galaxy_host = conf_orione.get('url', None)
            api_key = conf_orione.get('api_key', None)

            # NgLims client init
            gi = alta.galaxy.BioBlendObject(galaxy_host=galaxy_host, api_key=api_key)
            return gi.gi
        else:
            self.logger.error('Galaxy NgLims config not found')
            sys.exit()

    # Initializing connection to OMERO.biobank
    def __init_omero(self):
        conf_omero = self.conf.get_omero_section(subsection='default')
        if conf_omero:
            host = conf_omero.get('host', None)
            user = conf_omero.get('user', None)
            passwd = conf_omero.get('password', None)

            # OMERO.biobank client init
            kb = alta.biobank.BioBank(host, user, passwd).kb
            return kb
        else:
            self.logger.error('OMERO.biobank config not found')
            sys.exit()

    # Initializing connection to iRods
    def __init_irods(self):
        return alta.yrods.yRODS()


if __name__ == '__main__':
    main(sys.argv[1:])



