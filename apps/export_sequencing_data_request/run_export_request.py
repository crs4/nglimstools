import os
import subprocess
from datetime import datetime
import stat


class RunExportRequest:
    def __init__(self, api, conf, logger):
        self._conf = conf
        self._logger = logger
        self._orione = api.orione
        self._libraries = self._orione.gi.libraries
        self._export_request = None
        self._params = dict()

    def run(self, export_request, params=dict()):
        self._export_request = export_request
        self._params = params

        self._logger.info("Setting request status as RUNNING")
        self._export_request.set_as_running()

        self._logger.info("Preparing to run")
        self._prepare_run(export_mode=self._export_request.get_export_mode())

        self._logger.info("Creating RSYNC script")
        rsync_script = self._get_rsync_script(paths=self._export_request.get_export_paths(),
                                              export_mode=self._export_request.get_export_mode())

        self._logger.info("Running RSYNC script")
        rsync_log, rsync_script_filename = self._launch_script(script_sh=rsync_script, step='RSYNC')

        self._logger.info("{}".format(rsync_script_filename))

        self._logger.info("Creating POLLING script")
        polling_script = self._get_polling_script(export_mode=self._export_request.get_export_mode(),
                                                  qsub_log=rsync_log,
                                                  script_filename=rsync_script_filename)

        self._logger.info("Running POLLING script")
        polling_log, polling_script_filename = self._launch_script(script_sh=polling_script, step='POLLING')

        self._logger.info("{}".format(polling_script_filename))

    def _launch_script(self, script_sh, step):
        qsub_name, script_filename = self._prepare_qsub(export_mode=self._export_request.get_export_mode(),
                                                        step=step)

        self._save_script_file(script_filename=script_filename, script_sh=script_sh)

        qsub_args = self._get_qsub_args(script_filename=script_filename,
                                        qsub_name=qsub_name,
                                        export_mode=self._export_request.get_export_mode(),
                                        step=step)

        log = self._run(qsub_args=qsub_args)

        return log, script_filename

    def _get_qsub_args(self, script_filename, qsub_name, export_mode, step):
        conf_qsub = self._conf.get_section('qsub')
        qsub_args = list()

        if step in ['RSYNC'] and export_mode in ['FTP']:
            qsub_args.extend(['qsub', '-cwd', '-b', 'y', '-V', '-q', 'tanit', '-l', 'mamuthone=1', '-N',  qsub_name, '-o',
                              conf_qsub.get('log_folder'), '-e', conf_qsub.get('log_folder'), script_filename])

        elif step in ['RSYNC'] and export_mode in ['LIBRARY']:
            qsub_args.extend(['qsub', '-q', 'galaxy.upload', '-N', qsub_name, '-o',
                              conf_qsub.get('log_folder'), '-e', conf_qsub.get('log_folder'), script_filename])

        elif step in ['RSYNC'] and export_mode in ['MOUNT']:
            qsub_args.extend(['qsub', '-q', 'galaxy.upload', '-V', '-N', qsub_name, '-o',
                              conf_qsub.get('log_folder'), '-e', conf_qsub.get('log_folder'), script_filename])

        elif step in ['POLLING'] and export_mode in ['FTP']:
            qsub_args.extend(['qsub', '-cwd', '-b', 'y', '-V', '-q', 'tanit', '-l', 'mamuthone=1', '-N',  qsub_name, '-o',
                             conf_qsub.get('log_folder'), '-e', conf_qsub.get('log_folder'), script_filename])

        elif step in ['POLLING'] and export_mode in ['LIBRARY']:
            qsub_args.extend(['qsub', '-cwd', '-b', 'y', '-V', '-q','tanit','-l', 'mamuthone=1', '-N', qsub_name, '-o',
                             conf_qsub.get('log_folder'), '-e', conf_qsub.get('log_folder'), script_filename])

        elif step in ['POLLING'] and export_mode in ['MOUNT']:
            qsub_args.extend(['qsub', '-cwd', '-b', 'y', '-V', '-q','tanit','-l', 'mamuthone=1', '-N', qsub_name, '-o',
                             conf_qsub.get('log_folder'), '-e', conf_qsub.get('log_folder'), script_filename])

        return qsub_args

    def _run(self, qsub_args):
        def _make_call(qsub_args, logger):
            try:
                logger.debug('executing: {}'.format(qsub_args))
                output = subprocess.check_output(qsub_args)  # raises CalledProcessError
                if output:
                    logger.debug('command output: {}'.format(output))
                return output

            except subprocess.CalledProcessError as e:
                logger.debug(e)
                if e.output:
                    logger.debug("command output: {}".format(e.output))
                else:
                    logger.debug("no command output available")
                raise BaseException(e)

        qsub_log = _make_call(qsub_args=qsub_args, logger=self._logger)
        self._logger.info("{} submitted!".format(qsub_log))
        return qsub_log

    def _get_polling_script(self, export_mode, qsub_log, script_filename):
        polling_script = list()
        polling_script.append('#!/bin/bash\n')

        conf_qsub = self._conf.get_section('qsub')
        launcher = ""

        if export_mode in ['FTP']:
            launcher = "{} --id-request {} --ftp-customer {} --ftp-password {} " \
                       "--polling-rsync-script {} --polling-rsync-jobid {} --config-file {}". \
                format(conf_qsub.get('polling_script'),
                       self._export_request.get_id_request(),
                       self._params.get('ftp_customer'),
                       self._params.get('ftp_password'),
                       script_filename, qsub_log.split(' ')[2],
                       self._params.get('config_file'))

        elif export_mode in ['LIBRARY']:
            launcher = "{} --id-request {}  --polling-rsync-script {} --polling-rsync-jobid {} --config-file {}". \
                format(conf_qsub.get('polling_script'),
                       self._export_request.get_id_request(),
                       script_filename, qsub_log.split(' ')[2],
                       self._params.get('config_file'))

        elif export_mode in ['MOUNT']:
            launcher = "{} --id-request {} --mount-path {} " \
                       "--polling-rsync-script {} --polling-rsync-jobid {} --config-file {}". \
                format(conf_qsub.get('polling_script'),
                       self._export_request.get_id_request(),
                       self._params.get('mount_path'),
                       script_filename, qsub_log.split(' ')[2],
                       self._params.get('config_file'))

        polling_script.append(launcher)

        return polling_script

    def _get_rsync_script(self, paths=list(), export_mode=None):
        rsync_script = list()
        for path in paths:
            source_path = path.get('source')
            dest_path = self._get_destination_path(dest_path=path.get('destination'),
                                                   export_mode=export_mode)
            if dest_path:
                rsync_command = self._get_rsync_command(source_path=source_path,
                                                        dest_path=dest_path,
                                                        export_mode=export_mode)
                rsync_script.append(rsync_command)

                if export_mode == 'MOUNT':
                    chmod_cmd = "chmod 777 {}\n".format(dest_path)
                    rsync_script.append(chmod_cmd)

        return rsync_script

    def _get_rsync_command(self, source_path, dest_path, export_mode):
        rsync_command = None
        if export_mode == 'FTP':
            rsync_command = "rsync -avcPh --rsh=ssh {} {} \n".format(source_path, dest_path)

        elif export_mode == 'LIBRARY':
            rsync_command = "rsync -avcPh {} {} \n".format(source_path, dest_path)

        elif export_mode == 'MOUNT':
            rsync_command = "rsync -avcPh {} {} \n".format(source_path, dest_path)

        if rsync_command:
            self._logger.info("launching {}".format(rsync_command))

        return rsync_command

    def _get_destination_path(self, dest_path, export_mode):
        def _create_folder(_destination_path):
            folder = os.path.dirname(_destination_path)
            os.umask(0)
            if not os.path.exists(folder):
                os.makedirs(folder, 0755)

        destination_path = None
        if export_mode == 'FTP':
            conf = self._params['conf']
            destination_path = "{}:{}".format(conf.get('host'),
                                               os.path.join(conf.get('folder'),
                                                            self._params.get('ftp_customer'),
                                                            self._export_request.get_export_fcid(),
                                                            dest_path))

        elif export_mode == 'LIBRARY':
            conf = self._params['conf']
            destination_path = os.path.join(conf.get('folder'),
                                            self._export_request.get_export_fcid(),
                                            dest_path)
            _create_folder(destination_path)

        elif export_mode == 'MOUNT':
            destination_path = os.path.join(self._params.get('mount_path'),
                                            self._export_request.get_export_fcid(),
                                            dest_path)
            _create_folder(destination_path)

        return destination_path

    def _save_script_file(self, script_filename, script_sh):
        f = open(script_filename, 'w')
        f.writelines(script_sh)
        f.close()
        st = os.stat(script_filename)
        os.chmod(script_filename, st.st_mode | stat.S_IEXEC)
        f = open(script_filename, 'w')
        f.writelines(script_sh)
        f.close()
        st = os.stat(script_filename)
        os.chmod(script_filename, st.st_mode | stat.S_IEXEC)

    def _prepare_qsub(self, export_mode, step):
        qsub_name = "{}_{}_{}_{}".format(step,
                                         export_mode,
                                         self._export_request.get_export_description().replace('-', '_').replace(' ',
                                                                                                                 ''),
                                         datetime.now().strftime('%Y-%m-%d_%H_%M_%S.%f'))

        script_filename = "./{}.sh".format(qsub_name)

        return qsub_name, script_filename

    def _prepare_run(self, export_mode):

        def _ssh_cmd(host, cmd):
            ssh = subprocess.Popen(["ssh", host, cmd],
                                   shell=False,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)

            result = ssh.stdout.readlines()
            error = ssh.stderr.readlines()

            if error:
                self._logger.warning(error)
                raise
            else:
                return result

        prepared = dict()
        conf_export = self._conf.get_section('export')

        if 'LIBRARY' in export_mode and 'library' in conf_export:
            self._params['conf'] = conf_export.get('library', None)
            pass

        elif 'FTP' in export_mode and 'ftp' in conf_export:
            self._params['conf'] = conf_export.get('ftp', None)
            dest_folder = os.path.join(self._params['conf'].get('folder'),
                                       self._params['ftp_customer'],
                                       self._export_request.get_export_fcid())

            cmd = "mkdir -p  {}".format(dest_folder)
            result = _ssh_cmd("{}@{}".format(self._params['conf'].get('user'),
                                             self._params['conf'].get('host')),
                              cmd)

            prepared = dict(dest_folder=dest_folder, result=result)

        elif 'MOUNT' in export_mode:
            if not os.path.exists(self._params['mount_path']):
                self._logger.critical('Mount path doesn\'t exists')
                raise

        return prepared
