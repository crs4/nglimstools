import os
import json


class ExportRequest:
    def __init__(self, api, logger):
        self._logger = logger
        self._nglims = api.nglims
        self._omero = api.omero
        self._irods = api.irods
        self._request = dict()
        self._data_samples = dict()
        self._id_request = None

    def init(self, id_request):
        self._id_request = id_request
        self._logger.info("Searching export request with id {} from NGLims".format(self._id_request))
        self._request = self.get_export_request(id_request=self._id_request)

        if self._request:
            samples = self.get_samples()
            self._data_samples = self.get_data_samples(samples=samples)
            self._logger.info("Found {} export request with {} samples".format(self.get_export_description(), len(self._data_samples)))
        else:
            self._logger.critical("Missing request with id {}".format(self._id_request))

    def get_data_samples(self, samples=None):
        if samples:
            data_samples = dict()
            for sample, custom_name in samples.iteritems():
                if sample not in data_samples:
                    data_samples[sample] = list()
                    tube = self._omero.get_by_label(self._omero.Tube, sample)
                    seq_data_samples = self._omero.get_seq_data_samples_by_tube(tube)

                    for ds in seq_data_samples:
                        data_objects = self._omero.get_data_objects(ds)
                        for dobj in data_objects:

                            if 'fastq' in dobj.mimetype:
                                irods_path = dobj.path
                                phys_path = self._get_physical_path(irods_path)

                                if irods_path and phys_path:
                                    metadata = self._get_metadata(irods_path)
                                    destination_path = self._get_destination_path(metadata, phys_path, custom_name)

                                    data_sample = dict(
                                        irods_path=irods_path,
                                        phys_path=phys_path,
                                        metadata=metadata,
                                        dest_path=destination_path)

                                    data_samples[sample].append(data_sample)
            self._data_samples = data_samples
        return self._data_samples

    def get_export_paths(self):
        paths = list()
        for sample, data_sample in self._data_samples.iteritems():
            for data in data_sample:
                paths.append(dict(source=data['phys_path'], destination=data['dest_path']))

        return paths

    def get_samples(self):
        samples = dict()
        export_samples = self.get_export_samples()
        flowcell_id = self.get_export_fcid()

        if export_samples and flowcell_id:
            flowcell_complete_details = self._nglims.flowcell_complete_details(flowcell_id)
            if 'details' in flowcell_complete_details:
                for details in flowcell_complete_details['details']:
                    if details['name'] not in samples and details['name'] in export_samples:
                        samples[details['name']] = details['description']
        return samples

    def get_export_request(self, id_request=None):
        if id_request:
            self._request = self._nglims.get_export_request(id_request)
        return self._request

    def get_export_samples(self):
        if self._request:
            samples = self._request.get('object_ids', None)
            if samples:
                return json.loads(samples)
        return None

    def get_export_description(self):
        if self._request:
            return self._request.get('export_description', None)
        return None

    def get_export_contact(self):
        if self._request:
            return self._request.get('export_contact', None)
        return None

    def get_export_mode(self):
        if self._request:
            return self._request.get('export_mode', None)
        return None

    def get_export_status(self):
        if self._request:
            return self._request.get('export_status', None)
        return None

    def get_export_details(self):
        if self._request:
            return self._request.get('export_details', None)
        return None

    def get_export_fcid(self):
        description = self.get_export_description()
        if description:
            fcid = description.split('-')[0].strip()
            if self._nglims.exists_flowcell_id(fcid):
                return fcid
        return None

    def get_id_request(self):
        return self._id_request

    def set_as_running(self, id_request=None):
        if id_request:
            self._nglims.update_export_request_status(id_request, "RUNNING")
        else:
            self._nglims.update_export_request_status(self._id_request, "RUNNING")

    def set_as_successfully(self, id_request=None):
        if id_request:
            self._nglims.update_export_request_status(id_request, "SUCCESSFUL")
        else:
            self._nglims.update_export_request_status(self._id_request, "SUCCESSFUL")

    def set_as_failed(self, id_request=None):
        if id_request:
            self._nglims.update_export_request_status(id_request, "FAILED")
        else:
            self._nglims.update_export_request_status(self._id_request, "FAILED")

    def _get_physical_path(self, irods_path):
        f = self._irods.get_irods_file(irods_path)
        if f:
            phys_path = f.getPath()
            if phys_path and os.path.exists(phys_path):
                return phys_path
        return None

    def _get_metadata(self, irods_path):
        return self._irods.get_irods_metadata(irods_path)

    def _get_destination_path(self, metadata, phys_path, customer_sample_name):
        if 'read' in metadata:
            read = metadata['read']['value']
            phys_basename = os.path.basename(phys_path)
            basename = "{}_R{}".format(customer_sample_name, read)
            if len(os.path.splitext(phys_basename)) == 2:
                basename = "{}{}".format(basename, os.path.splitext(phys_basename)[1])
            return basename
        return None



