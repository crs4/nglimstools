import os
from PyPDF2 import PdfFileReader

class Register:
    def __init__(self, nglims, conf, logger):
        self.conf = conf
        self.logger = logger
        self.nglims = nglims
        self.legend = self.conf.get_section('report_legend')
        self.quality_check_keys = self.conf.get_section('quality_check')['indexes']
        self.quality_check_paths = self.conf.get_section('quality_check')['paths']

    def save_flowcell_status_report(self, flowcell_status_report):
        for sample_status in flowcell_status_report:
            self._save_sample_status_report(sample_status=sample_status)
            self._save_fastqc_report(sample_status=sample_status)

    def _save_sample_status_report(self, sample_status):
        run_dir = sample_status.get('run_dir', None)
        fcid = run_dir[-9:]

        sample_name = self._get_sample_name(sample_status=sample_status)
        request_name = "{} {}".format(fcid, sample_name)

        # data_to_save = dict(
        #     run_dir=run_dir,
        #     sample_name=sample_name,
        #     status=self._retrieve_boolean(sample_status['status']),
        #     sequencing=self._retrieve_boolean(sample_status['sequencing_status']),
        #     nglims=self._retrieve_boolean(sample_status['nglims_status']),
        #     omero=self._retrieve_boolean(sample_status['omero_status']),
        #     irods=self._retrieve_boolean(sample_status['irods_status']),
        #     quality_check=self._retrieve_boolean(sample_status['qc_status']),
        #     name=request_name
        # )

        data_to_save = dict(
            run_dir=run_dir,
            sample_name=sample_name,
            status=self._retrieve_boolean(sample_status['status']),
            seq=self._retrieve_boolean(sample_status['sequencing_status']),
            nglims=self._retrieve_boolean(sample_status['nglims_status']),
            stage1=self._retrieve_boolean(sample_status['omero_status']),
            qc=self._retrieve_quality_check(qc_status=sample_status['qc_status'], qc_file=sample_status['qc_file']),
            name=request_name
        )

        successfully, rsp = self.nglims.save_fc_status_report(data_to_save)

        if not successfully:
            self.logger.warning("{} FAILED - {}".format(sample_name, rsp))

    def _save_fastqc_report(self, sample_status):
        qc_file = sample_status['qc_file']

        if qc_file:
            run_dir = sample_status.get('run_dir', None)
            fcid = run_dir[-9:]

            sample_name = self._get_sample_name(sample_status=sample_status)
            fastqc_data = self._get_fastqc_data_from_pdf(qc_file=qc_file, fcid=fcid, sample_name=sample_name)

            successfully, rsp = self.nglims.save_fastqc_report(fastqc_data)

            if not successfully:
                self.logger.warning("{} FAILED - {}".format(sample_name, rsp))

    def _get_fastqc_data_from_pdf(self, qc_file, fcid, sample_name):
        f = open(qc_file, 'rb')
        reader = PdfFileReader(f)
        contents = reader.getPage(0).extractText().split('\n')
        fastqc_data = dict(sequences_report=' ', name="{} {}".format(fcid, sample_name))

        for line in contents:

            for key in self.quality_check_keys.get('keys', []):
                if key in line:
                    k = key.lower().replace(' ', '_').replace('%', '')
                    if key in self.quality_check_keys.get('int', []):
                        fastqc_data[k] = int(line.replace(key, ''))
                    elif key in self.quality_check_keys.get('rindex', []):
                        line = line[line.rindex(key):]
                        fastqc_data[k] = line.replace(key, '')
                    else:
                        fastqc_data[k] = line.replace(key, '')

        return fastqc_data

    def _retrieve_quality_check(self, qc_status=False, qc_file=None):
        if not qc_status:
            return self._retrieve_boolean(qc_status)
        else:
            filename = os.path.basename(qc_file)
            chm = "--chmod=a+r"
            cmd = "rsync {} {} {}".format(chm, qc_file, self.quality_check_paths.get('destination'))
            try:
                os.system(cmd)
            except ValueError:
                pass

            return os.path.join(self.quality_check_paths.get('prefix', 'qc'), filename)

    def _retrieve_boolean(self, flag):
        if flag:
            return u"\u2714"
        return u"\u2718"

    def _get_sample_name(self, sample_status):
        sample = sample_status.get('sample')

        if not sample:
            sample = self.legend.get('None')

        return sample


