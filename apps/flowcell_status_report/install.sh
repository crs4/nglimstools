#!/bin/bash

INIT_FILE="./init_file.yaml"
LOCAL_TMP_DIR=$(mktemp -t repos -d)
HERE=$(pwd)

#usage: parse_yaml INIT_FILE [prefix]
function parse_yaml {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

# MAIN #
echo "LOG: Creating temp directory"
eval $(parse_yaml $INIT_FILE)

# clone repository
echo "LOG: Cloning repository"
cd $LOCAL_TMP_DIR
git clone $REPOS_HOST
cd $REPOS_NAME
git checkout $BRANCH

# copy scripts
echo "LOG: Copying scripts"
rsync  -vcur --exclude=install.sh $LOCAL_TMP_DIR/$REPOS_NAME/$SCRIPTS_ORIGIN  $SCRIPTS_DEST

echo "LOG: Removing temp directory"
rm -rf $LOCAL_TMP_DIR

