import os
import time
import datetime
from automator import illumina_run_dir
from save_flowcell_status_report import Register
# FIXME: implemt alta method
import automator.agent.irods as irods


class AnalyzeRundDir:
    def __init__(self, api, conf, logger):
        self.conf = conf
        self.logger = logger
        self.nglims = api.nglims
        self.omero = api.omero
        self.irods = api.irods
        self.expiration_day = None

    def analyze_rundir(self, last_runs=False):
        if last_runs:
            self.expiration_day = self.conf.get_section('params').get('time_range', 0)

        self.analyze_running_rundir()
        self.analyze_completed_rundir(last_runs)

    def analyze_running_rundir(self):
        self.logger.info("Looking for still running flowcell")

        # Iterate on running run directories
        rd_list = self._get_rundir_list(status='running')

        for rd in rd_list:
            flowcell_status_report = list()

            # Print on stdout the run directory of the current flowcell
            self.logger.info(rd)

            # Flowcell ID
            flowcell_id = self._fc_from_rd(rd)

            # Check if the flowcell samplesheet is registered in nglims
            self.logger.info('Checking if the flowcell samplesheet is registered in nglims')
            nglims_status, samples = self._check_nglims_flowcell_status(flowcell_id=flowcell_id)

            # Check if the sequencing process is finished
            self.logger.info('Checking if the sequencing process is finished')
            sequencing_status, run_info = self._check_sequencing_flowcell_status(run_dir=rd, status='running')

            # Build report row
            self.logger.info('Building report rows')
            row = self._build_row(run_dir=rd, nglims_status=nglims_status, sequencing_status=sequencing_status)

            flowcell_status_report.append(row)

            self.logger.info('Saving flowcell status report')
            register = Register(nglims=self.nglims, conf=self.conf, logger=self.logger)
            register.save_flowcell_status_report(flowcell_status_report=flowcell_status_report)

    def analyze_completed_rundir(self, last_runs=False):
        self.logger.info("Looking for completed flowcell")

        # Iterate on completed run directories
        rd_list = self._get_rundir_list(status='completed')

        first_run = self._get_first_run(last_runs=last_runs)

        for rd in rd_list:

            if self._is_last(this_run=rd, first_run=first_run):
                break

            flowcell_status_report = list()

            # Print on stdout the run directory of the current flowcell
            self.logger.info(rd)

            # Flowcell ID
            flowcell_id = self._fc_from_rd(rd)

            # Check if the flowcell samplesheet is registered in nglims
            self.logger.info('Checking if the flowcell samplesheet is registered in nglims')
            nglims_status, samples = self._check_nglims_flowcell_status(flowcell_id=flowcell_id)

            if not nglims_status:
                self.logger.info('Missing samplesheet - skipping this run')
                continue

            # Check if the sequencing process is finished
            self.logger.info('Checking if the sequencing process is finished')
            sequencing_status, run_info = self._check_sequencing_flowcell_status(run_dir=rd, status='completed')

            if not run_info:
                self.logger.info('Missing run information - skipping this run')
                continue

            # Check the flowcell status in OMERO
            self.logger.info('Checking the flowcell status in OMERO')
            omero_status = self._check_omero_flowcell_status(run_info=run_info)

            # Check the flowcell status in IRODS
            self.logger.info('Checking the flowcell status in IRODS')
            irods_status = self._check_irods_flowcell_status(omero_status)

            # Build report rows
            self.logger.info('Building sample report rows')
            for sample in samples:
                if sample in omero_status:
                    row = self._build_row(run_dir=rd, sample=sample,
                                          sequencing_status=sequencing_status,
                                          nglims_status=nglims_status,
                                          omero_status=omero_status[sample]['status'],
                                          irods_status=irods_status[sample]['status'],
                                          qc_status=irods_status[sample]['quality_check_status'])
                    if len(irods_status[sample]['quality_check']):
                        row.update(qc_file=irods_status[sample]['quality_check'].pop())
                else:
                    row = self._build_row(run_dir=rd, nglims_status=nglims_status,
                                          sequencing_status=sequencing_status)

                flowcell_status_report.append(row)

            self.logger.info('Saving flowcell status report')
            register = Register(nglims=self.nglims, conf=self.conf, logger=self.logger)
            register.save_flowcell_status_report(flowcell_status_report=flowcell_status_report)

    def _build_row(self, run_dir, sample=False,
                   sequencing_status=False, nglims_status=False,
                   omero_status=False, irods_status=False,
                   qc_status=False, qc_file=False,
                   status=False):

        if sequencing_status and nglims_status and omero_status and irods_status and qc_status:
            status = True

        row = dict(run_dir=run_dir, sample=sample,
                   sequencing_status=sequencing_status, nglims_status=nglims_status,
                   omero_status=omero_status, irods_status=irods_status,
                   qc_status=qc_status, qc_file=qc_file,
                   status=status)

        return row

    def _check_irods_flowcell_status(self, omero_status):
        irods_status = IrodsStatus(irods=self.irods,
                                   conf=self.conf, logger=self.logger)

        return irods_status.check_flowcell_status(omero_status=omero_status)

    def _check_omero_flowcell_status(self, run_info):
        omero_status = OmeroStatus(omero=self.omero, conf=self.conf, logger=self.logger)

        return omero_status.check_flowcell_status(flowcell_id=run_info.flowcell_id, reads=run_info.get_num_reads(), is_indexed=run_info.is_indexed())

    def _check_sequencing_flowcell_status(self, run_dir, status):
        path = self.conf.get_section('sequencinq_data_path')
        if path:
            rd_full_path = os.path.join(path[status], run_dir)
            if status == 'completed':
                rd_full_path = os.path.join(rd_full_path, 'raw')
            if os.path.exists(rd_full_path):
                rund_dir_obj = illumina_run_dir.RunDir(rd_full_path)
                return rund_dir_obj.is_finished(), rund_dir_obj.get_run_info()
        return False, None

    def _check_nglims_flowcell_status(self, flowcell_id):
        if self.nglims.exists_flowcell_id(flowcell_id):
            samples = set()
            for lane in self.nglims.flowcell_details(flowcell_id)['lanes']:
                for slot in lane['slots']:
                    samples.add(slot['sample_label'])
            return True, samples
        return False, None

    def _get_rundir_list(self, status='running'):
        path = self.conf.get_section('sequencinq_data_path')
        if path and os.path.exists(path[status]):
            return reversed(sorted([s for s in os.listdir(path[status]) if s.startswith('1')]))
        return list()

    def _fc_from_rd(self, rd):
        return rd[-9:]

    def _get_first_run(self, last_runs):
        rd_list = self._get_rundir_list(status='completed')
        runs = list(rd_list)

        if last_runs and len(runs) > 0:
            return runs[0]

        return False

    def _is_last(self, this_run, first_run):

        if first_run and this_run[:1].isdigit():
            run_1 = "20{}-{}-{} 00:00:00".format(this_run[0:2], this_run[2:4], this_run[4:6])
            run_2 = "20{}-{}-{} 00:00:00".format(first_run[0:2], first_run[2:4], first_run[4:6])

            aa = time.strptime(run_1, '%Y-%m-%d %H:%M:%S')
            aaa = datetime.datetime.fromtimestamp(time.mktime(aa))

            bb=time.strptime(run_2, '%Y-%m-%d %H:%M:%S')
            bbb = datetime.datetime.fromtimestamp(time.mktime(bb))

            days_beetween_runs = (aaa-bbb).days

            if abs(int(days_beetween_runs)) > self.expiration_day:
                return True

        return False


class IrodsStatus:
    def __init__(self, irods, conf, logger):
        self.logger = logger
        self.conf = conf
        self.irods = irods
        self._status = dict()

    def check_flowcell_status(self, omero_status):

        for sample in omero_status.keys():
            sample_status = self._check_sample_status(sample, omero_status)
            self._status[sample] = sample_status

        return self._status

    def _check_sample_status(self, sample, omero_status):
        sample_status = dict(quality_check=[], quality_check_status=False, reads=[], status=False)
        for irods_path in omero_status[sample]['reads']:
            path = self._get_physical_path(irods_path=irods_path, context='reads')
            if path and os.path.exists(path):
                sample_status['reads'].append(path)

        if len(sample_status.get('reads', [])) == len(omero_status[sample].get('reads', [])):
            sample_status['status'] = True

        for irods_path in omero_status[sample]['quality_check']:
            # FIXME: implement alta method
            #path = self.irods.get_physical_path(irods_path)
            path = self._get_physical_path(irods_path=irods_path, context='quality_check')

            if path and os.path.exists(path):
                sample_status['quality_check'].append(path)
                sample_status['quality_check_status'] = True

        return sample_status

    def _get_physical_path(self, irods_path, context='reads'):
        if context == 'reads':
            f = self.irods.get_irods_file(irods_path)
            if f:
                return f.getPath()
        elif context == 'quality_check':

            if len(self.irods.get_irods_metadata(irods_path)) > 0:
                info = irods.get_object_info(irods_path)
                path = info.get('phys_path', None)
                return path

        return None


class OmeroStatus:
    def __init__(self, omero, conf, logger):
        self.logger = logger
        self.conf = conf
        self.omero = omero
        self.reads = None
        self._status = dict()

    def check_flowcell_status(self, flowcell_id, reads, is_indexed):
        flowcell = self._get_flowcell(flowcell_id)

        if flowcell:
            samples = self._get_samples(flowcell)
            for sample in samples:
                sample_status = self._check_sample_status(sample, reads, is_indexed)
                self._status[sample.label] = sample_status

        return self._status

    def _check_sample_status(self, sample, reads, is_indexed):
        sample_status = dict(quality_check=[], reads=[], status=False)
        seq_data_samples = self.omero.get_seq_data_samples_by_tube(sample)

        for ds in seq_data_samples:
            data_objects = self.omero.get_data_objects(ds)

            for dobj in data_objects:

                if 'pdf' in dobj.mimetype:
                    sample_status['quality_check'].append(dobj.path)
                elif 'fastq' in dobj.mimetype:
                    sample_status['reads'].append(dobj.path)

        if is_indexed:
            reads -= 1

        if reads == len(sample_status.get('reads', [])):
            sample_status['status'] = True

        return sample_status

    def _get_flowcell(self, flowcell_id):
        return self.omero.get_container(flowcell_id)

    def _get_samples(self, flowcell):
        lanes = self.omero.get_lanes_by_flowcell(flowcell)
        slots = iter(slot for l in lanes for slot in self.omero.get_laneslots_by_lane(l))
        subsamples = set(s.action.target for s in slots)
        samples = set(self._get_original_sample(s) for s in subsamples)
        return samples

    def _get_original_sample(self, sample):
        if hasattr(sample.action, 'target'):
            return sample.action.target
        else:
            return sample
