#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import alta
from analyze_rundir import AnalyzeRundDir

sys.path.insert(0, os.path.dirname(__file__))


def make_parser():
    parser = argparse.ArgumentParser(description='Update flowcell status report')

    parser.add_argument('--config-file', '-c', type=str,
                        default='init_file.yaml', help='yaml config file')
    parser.add_argument('--last-runs', action='store_true', default=False, help='')
    parser.add_argument('--selected-runs', '-r', type=str,
                        help='list run file')
    parser.add_argument('--loglevel', type=str, choices=alta.LOG_LEVELS,
                        help='logging level (default: INFO)', default='INFO')
    parser.add_argument('--logfile', type=str,
                        help='log file (default=stderr)')

    return parser


def main(argv):
    def check_if_path_exists(path, logger):
        if not os.path.exists(path):
            logger.error("{} doesn't exists".format(path))
            sys.exit()

    parser = make_parser()
    args = parser.parse_args(argv)

    # Initializing logger
    logger = alta.a_logger('main', level=args.loglevel,
                           filename=args.logfile)

    if args.config_file:
        check_if_path_exists(args.config_file, logger)

    # Load YAML configuration file
    conf = alta.ConfigurationFromYamlFile(args.config_file)

    api = Client(conf=conf, logger=logger)

    analysis = AnalyzeRundDir(api=api, conf=conf, logger=logger)
    analysis.analyze_rundir(last_runs=args.last_runs)


class Client:
    def __init__(self, conf, logger):
        self.conf = conf
        self.logger = logger
        self.omero = self.__init_omero()
        self.nglims = self.__init_nglims()
        self.irods = self.__init_irods()

    # Initializing connection to NgLims
    def __init_nglims(self):
        conf_nglims = self.conf.get_galaxy_section(subsection='nglims')
        if conf_nglims:
            galaxy_host = conf_nglims.get('url', None)
            api_key = conf_nglims.get('api_key', None)

            # NgLims client init
            gi = alta.galaxy.NGLims(galaxy_host=galaxy_host, api_key=api_key)
            return gi.gi.nglims
        else:
            self.logger.error('Galaxy NgLims config not found')
            sys.exit()

    # Initializing connection to OMERO.biobank
    def __init_omero(self):
        conf_omero = self.conf.get_omero_section(subsection='default')
        if conf_omero:
            host = conf_omero.get('host', None)
            user = conf_omero.get('user', None)
            passwd = conf_omero.get('password', None)

            # OMERO.biobank client init
            kb = alta.biobank.BioBank(host, user, passwd).kb
            return kb
        else:
            self.logger.error('OMERO.biobank config not found')
            sys.exit()

    # Initializing connection to iRods
    def __init_irods(self):
        return alta.yrods.yRODS()


if __name__ == '__main__':
    main(sys.argv[1:])
